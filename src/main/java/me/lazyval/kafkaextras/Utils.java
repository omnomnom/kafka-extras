package me.lazyval.kafkaextras;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;

public class Utils {
    private Utils() {} // to prevent from instantiation

    public static final File TMP_DIR = new File(System.getProperty("java.io.tmpdir"));

    // TODO: make sure everything is closed properly
    public static int findRandomPort() throws IOException {
        ServerSocket socket = null;
        try {
            socket = new ServerSocket(0);
            return socket.getLocalPort();
        } finally {
            if (socket != null) socket.close();
        }
    }
}
