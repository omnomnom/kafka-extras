package me.lazyval.kafkaextras;

public class Brokers {
    private Brokers() {} // prevents from instantiating, as this object is meant to collect static methods

    public static EmbedKafkaBuilder kafkaBuilder() {
        return new EmbedKafkaBuilder();
    }
}
