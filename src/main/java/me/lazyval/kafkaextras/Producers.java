package me.lazyval.kafkaextras;

import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class Producers {
    public static void send(Iterable<String> messages, String topicName, String brokerStr) {
        Map<String, String> props = new HashMap<String, String>();
        props.put("metadata.broker.list", brokerStr);
        props.put("serializer.class", "kafka.serializer.StringEncoder");
        send(messages, topicName, props);
    }

    // TODO: use decoders to send not only Strings
    public static void send(Iterable<String> messages, String topicName, Map<String, String> customProps) {
        Properties props = new Properties();
        props.putAll(customProps);
        ProducerConfig config = new ProducerConfig(props);
        Producer<String, String> producer = new Producer<String, String>(config);

        List<KeyedMessage<String, String>> keyedMessages = new ArrayList<KeyedMessage<String, String>>();
        for (String message : messages) {
            keyedMessages.add(new KeyedMessage<String, String>(topicName, message));
        }

        producer.send(keyedMessages);
    }
}
