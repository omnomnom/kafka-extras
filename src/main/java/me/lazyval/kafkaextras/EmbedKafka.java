package me.lazyval.kafkaextras;

import kafka.server.KafkaConfig;
import kafka.server.KafkaServerStartable;
import scala.collection.JavaConversions;

import java.util.Map;
import java.util.Properties;

public class EmbedKafka {
    private final KafkaServerStartable server;

    EmbedKafka(EmbedKafkaBuilder builder) {
        Properties props = new Properties();
        props.put("port", String.valueOf(builder.port()));
        props.put("broker.id", String.valueOf(builder.port()));
        props.put("zookeeper.connect", builder.zkConnection());
        props.put("log.dirs", builder.dataDir().getAbsolutePath());

        Map<String, String> customProps = JavaConversions.asJavaMap(builder.customProperties());
        props.putAll(customProps);

        KafkaConfig config = new KafkaConfig(props);

        server = new KafkaServerStartable(config);
    }

    public void start() {
        server.startup();
    }

    public void stop() {
        server.shutdown();
        server.awaitShutdown();
    }
}
