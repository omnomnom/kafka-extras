package me.lazyval.kafkaextras

import org.apache.zookeeper.server.{NIOServerCnxn, ZooKeeperServer}
import java.net.InetSocketAddress
import java.io.File

class EmbedZookeeper(val clientPort: Int, dataDir: File) {
  val NumConnections = 5000
  val TickTime = 2000

  val server = new ZooKeeperServer(dataDir, dataDir, TickTime)
  val standaloneServerFactory = new NIOServerCnxn.Factory(new InetSocketAddress(clientPort), NumConnections)
  val connectionStr = "localhost:" + clientPort

  def start() {
    standaloneServerFactory.startup(server)
  }

  def stop() {
    standaloneServerFactory.shutdown()
  }
}

object EmbedZookeeper {
  val TempDirectory = new File(System.getProperty("java.io.tmpdir") + System.currentTimeMillis())
}
