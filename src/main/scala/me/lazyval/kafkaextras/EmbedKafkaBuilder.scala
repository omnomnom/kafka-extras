package me.lazyval.kafkaextras

import scala.collection.immutable.{Map => ImmutableMap}
import java.io.File

case class EmbedKafkaBuilder private[kafkaextras](
                              port: Int = 6667,
                              zkConnection: String = "localhost:2081",
                              autoCreateTopics: Boolean = true,
                              replicationFactor: Int = 1,
                              dataDir: File = new File("/tmp/kafka-logs"),
                              customProperties: ImmutableMap[String, String] = ImmutableMap.empty[String, String]) {

  def this() = {
    this(customProperties = ImmutableMap.empty[String, String])
  }

  def withPort(port: Int) = this.copy(port = port)
  def withRandomPort() = this.withPort(Utils.findRandomPort())
  def withZkConnection(connectionStr: String) = this.copy(zkConnection = connectionStr)
  def withAutoCreateTopics(autoCreate: Boolean) = this.copy(autoCreateTopics = autoCreate)
  def withReplicationFactor(replicaCount: Int) = this.copy(replicationFactor = replicaCount)
  def withDataDir(dir: File) = this.copy(dataDir = dir)
  def placeDataInTmpDir() = {
    val temporaryDir = new File(Utils.TMP_DIR, "kafka@" + this.port)
    // TODO: make sure automatic deletion is not evil
    // deleteOnExit burdens RAM with yet another 1k
    // http://puneeth.wordpress.com/2006/01/23/filedeleteonexit-is-evil/
    temporaryDir.deleteOnExit()
    this.copy(dataDir = temporaryDir)
  }
  def withProperty(name: String, value: String) = {
    val newProps: ImmutableMap[String, String] = customProperties + (name -> value)
    this.copy(customProperties = newProps)
  }

  def build(): EmbedKafka = {
    new EmbedKafka(this)
  }
}
